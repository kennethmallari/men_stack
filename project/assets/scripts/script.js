//Login Functionality with Ajax:
function validateLoginForm() {
  var emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  } else if (passwordVal == "") {
    alert("Please enter password");
    return false;
  } else if (passwordVal.length < 3) {
    alert("Password length must be atleast 3 characters");
    return false;
  }

  authenticate(emailVal, passwordVal);

  function authenticate(email, password) {
    $.ajax({
      type: "POST",
      url: "http://localhost:3000/users/authenticate",
      dataType: "json",
      data: {
        email: email,
        password: password,
      },
      success: function (data) {
        console.log(data);
        if (data) {
          localStorage.setItem("loggedInUser", JSON.stringify(data));
          setLoginSession("userId", data.userId, 30);
          return true;
        } else {
          alert("Wrong email or password. Please try again.");
          return false;
        }
      },
      async: false,
    });
  }
}

function getLoginSession(cname) {
  if (document.cookie.length > 0) {
    cookie =
      document.cookie.match("(^|;)\\s*" + cname + "\\s*=\\s*([^;]+)")?.pop() ||
      "";
    return cookie;
  }
  return "";
}

function setLoginSession(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function checkLoginSession() {
  let userId = getLoginSession("userId");
  if (userId !== null && userId !== "") {
    return userId;
  } else {
    return null;
  }
}

function deleteCookie(name) {
  setLoginSession(name, null, -1);
}

function logOut() {
  deleteCookie("userId");
  localStorage.removeItem("loggedInUser");
  return true;
}

//Register Functionality with Ajax:
function validateRegistrationForm() {
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var confirmPassword = document.getElementById("confirmPassword").value;

  var atPosition = email.indexOf("@");
  var dotPosition = email.lastIndexOf(".");

  if (fullName === "") {
    alert("Please enter your full name.");
    return false;
  } else if (email === "") {
    alert("Please enter your email.");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email.");
    return false;
  } else if (password === "") {
    alert("Please enter password.");
    return false;
  } else if (password.length < 3) {
    alert("Password length must be atleast 3 characters.");
    return false;
  } else if (confirmPassword === "") {
    alert("Please enter confirmation password.");
    return false;
  } else if (password !== confirmPassword) {
    alert("Password you entered didn't match.");
    return false;
  }

  var user = {
    userId: Date.now(),
    fullName: fullName,
    email: email,
    password: password,
  };

  $.ajax({
    type: "POST",
    url: "http://localhost:3000/users",
    data: user,
    success: function (response) {
      return true;
    },
    complete: function () {
      return true;
    },
  });
}

/*******  User's Read Update Delete: ********/

//Display users for UsersList Table dynamically using Ajax:

function diplayUsersAjax() {
  let data;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/users",
    success: function (response) {
      data = response;
      console.log(response);
    },
    async: false,
  });

  for (let i = 0; i < data.length; i++) {
    document.write(`
    <tr>
      <td class="primary-td">${data[i].fullName}</td>
      <td class= "td-center primary-td">${data[i].email}</td>
      <td class="td-center""><a href="./editUserPage.html?userid=${data[i]._id}">Edit</a> |
  <a
    href="#"
    class="delete-btn"
    data-bs-toggle="modal"
    data-bs-target="#exampleModal2"
    onclick="setFunction('${data[i]._id}')"
    >Delete</a
  ></td> 
    </tr>
    `);
  }
}

//For passing the ID to delete a user using AJAX:

function setFunction(id) {
  deleteUserID = 0;
  deleteUserID = id;
}

//Delete user using Ajax:
function deleteUser() {
  $.ajax({
    type: "DELETE",
    url: `http://localhost:3000/users/${deleteUserID}`,
    succes: function (response) {
      return true;
    },
    async: false,
  });
  location.href = "./usersList.html";
}

// For Populating the Edit User Page:

function populateEditPage() {
  const nameField = document.getElementById("full-name-field");
  const emailField = document.getElementById("email-field");
  let params = new URLSearchParams(window.location.search);
  let userId = params.get("userid");
  let data;

  console.log(nameField);

  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/${userId}`,
    success: function (response) {
      data = response;
      console.log(response);
    },
    async: false,
  });

  nameField.value = data.fullName;
  emailField.value = data.email;
}
//Updating the User Information:
function validateUpdateFormAjax() {
  var fullNameVal = document.getElementById("full-name-field").value;
  var emailVal = document.getElementById("email-field").value;
  let params = new URLSearchParams(window.location.search);
  let userId = params.get("userid");

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (fullNameVal == "") {
    alert("Please enter full name");
    return false;
  } else if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  }
  let user = {
    fullName: fullNameVal,
    email: emailVal,
  };

  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/users/${userId}`,
    data: user,
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

//get users from the local storage:
function getUsers() {
  let users;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/users",
    success: function (response) {
      users = response;
      console.log(users);
    },
    async: false,
  });

  if (!users) {
    return [];
  } else {
    return users;
  }

  //return users;
  // let usersRaw = localStorage.getItem("users");
  // let users = [];
  // if (!usersRaw) {
  //   users = [];
  //   return users;
  // } else {
  //   users = JSON.parse(usersRaw);
  //   return users;
  // }
}

//get the registred user from the local storage:
function getUser() {
  const userRaw = localStorage.getItem("loggedInUser");
  const user = JSON.parse(userRaw);
  return user;
}

/******** Functions for Uploads:  ********/
//create uploads array of objects:
// function createUploadFile() {
//   let upload = {};
//   let uploads = [];

//   let uploadId = Date.now();
//   let label = document.getElementById("labelInput");
//   let user = JSON.parse(localStorage.getItem("loggedInUser"));

//   upload.uploadId = uploadId;
//   upload.label = label.value;
//   upload.userId = user.id;
//   upload.sharedTo = [];
//   upload.fileName = "sample document";

//   if (!localStorage.getItem("uploads")) {
//     uploads.push(upload);
//     localStorage.setItem("uploads", JSON.stringify(uploads));
//     return true;
//   } else {
//     uploads = JSON.parse(localStorage.getItem("uploads"));
//     uploads.push(upload);
//     localStorage.setItem("uploads", JSON.stringify(uploads));
//     return true;
//   }
// }

//get the uploads from the DB:
function getUploads(userId) {
  let uploads = [];

  if (userId !== null) {
    $.ajax({
      type: "GET",
      url: "http://localhost:3000/uploads/myUploads",
      data: { userId: userId },
      success: function (response) {
        uploads = response;
        console.log(response);
      },
      async: false,
    });
  } else {
    $.ajax({
      type: "GET",
      url: "http://localhost:3000/uploads/allUploads",
      success: function (response) {
        uploads = response;
        console.log(response);
      },
      async: false,
    });
  }
  return uploads;
}

function uploadModal() {
  const modal = document.querySelector("#uploadModal");
  const span = document.querySelector("#closeUpload");
  const form = document.querySelector("#uploadForm");
  const btnSave = document.querySelector("#save");
  const btnCancel = document.querySelector("#cancel");
  const inputFile = document.querySelector("#realButton");
  const btnChooseFile = document.querySelector("#chooseFile");
  const fileName = document.querySelector("#fileName");

  modal.style.display = "block";
  let fN = fileName.innerHTML;
  span.onclick = function () {
    modal.style.display = "none";
  };

  btnChooseFile.onclick = function () {
    modal.style.display = "block";
    //for you to trigger the input file:
    inputFile.click();
  };

  inputFile.onchange = function () {
    if (inputFile.value) {
      modal.style.display = "block";
      //to get the filename:
      fileName.innerHTML = inputFile.value.match(
        /[\/\\]([\w\d\s\.\-\(\)]+)$/
      )[1];
    }
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  btnSave.onclick = function () {
    let formData = new FormData(form);
    let sharedTo = [];
    formData.append("userId", getLoginSession("userId"));
    formData.append("uploadId", Date.now());
    formData.append("sharedTo", sharedTo);

    console.log("This is form Data: ", formData);

    $.ajax({
      type: "POST",
      url: "http://localhost:3000/uploads",
      data: formData,
      dataType: "json",
      contentType: false,
      processData: false,
    });
    modal.style.display = "none";
  };
}

function displayUploadsTable() {
  const loggedInUser = getLoginSession("userId");

  let uploads = getUploads(loggedInUser);
  const table = document
    .getElementById("uploadsTable")
    .getElementsByTagName("tbody")[0];

  if (uploads.length == 0) {
    table.innerHTML =
      "<tr><td class='alignCenter' colspan='3'>No Uploads...</td></tr>";
  } else {
    console.log("this.post: ", this.post);
    uploads.forEach((upload) => {
      let anchor = document.createElement("a");
      let row = table.insertRow();
      let cell1 = row.insertCell(0);
      let cell2 = row.insertCell(1);
      let cell3 = row.insertCell(2);
      let fileName = document.createTextNode(upload.fileName);
      anchor.appendChild(fileName);
      anchor.href = `../../uploads/${upload.fileName}`;
      let newLabel = document.createTextNode(upload.label);

      console.log(anchor.href);

      let actions = `<a class='noDeco' href='#' onclick='editUploadModal(${upload.uploadId}, "${upload.label}");'>Edit</a> |
       <a class='noDeco' href='#' onclick='deleteUpload(${upload.uploadId}, "${upload.fileName}"); '>Delete</a> |  
       <a class='noDeco' href='./sharedPage.html?uploadId=${upload.uploadId}&name=${upload.fileName}'>Share</a>`;

      console.log(actions);

      console.log(anchor);
      cell1.appendChild(newLabel);
      cell2.appendChild(anchor);
      cell3.innerHTML += actions;
      cell2.className = "alignCenter";
      cell3.className = "alignCenter";
    });
  }
}

function editUploadModal(uploadId, label) {
  console.log(label);
  //viewUploadInfo(uploadId);
  const modal = document.querySelector("#editModal");
  const span = document.querySelector("#closeEdit");
  const btnUpdate = document.querySelector("#btnUpdate");
  const btnCancel = document.querySelector("#btn-edit-cancel");
  const newLabel = document.querySelector("#editLabelInput");
  modal.style.display = "block";

  newLabel.value = label;

  span.onclick = function () {
    modal.style.display = "none";
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  btnUpdate.onclick = function () {
    if (newLabel.value !== "") {
      modal.style.display = "none";
      const upload = { uploadId: uploadId, label: newLabel.value };
      $.ajax({
        type: "PUT",
        url: `http://localhost:3000/uploads/`,
        data: upload,
        success: function (response) {
          return true;
        },
        async: false,
        complete: function () {
          return true;
        },
      });
    } else {
      alert("Please add Label");
    }
  };
}

function deleteUpload(id, fileName) {
  const modal = document.querySelector("#deleteModal");
  const span = document.querySelector("#closeDelete");
  const btnOk = document.querySelector("#btnOk");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };
  btnOk.onclick = function () {
    modal.style.display = "none";
    $.ajax({
      type: "DELETE",
      url: `http://localhost:3000/uploads/`,
      data: { uploadId: id, fileName: fileName },
      succes: function (response) {
        return true;
      },
      async: false,
    });
    location.href = "./manageDocumentPage.html";
  };
}

function getUserInfo(id) {
  let user;

  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/api/${id}`,
    success: function (data) {
      if (data) {
        user = data;
      }
    },
    async: false,
  });
  return user;
}

/**** For Shared Page: ****/
//For dropdown
function showRegisteredUsers() {
  let dropdown = document.querySelector("#usersDropdown");
  let users = getUsers();
  let activeUserId = getLoginSession("userId");

  for (let user = 0; user < users.length; user++) {
    if (users[user].userId != activeUserId) {
      let option = document.createElement("option");
      option.value = users[user].userId;
      option.innerHTML = users[user].fullName;
      dropdown.appendChild(option);
    }
  }
}

function populateSharedUsers(uploadId) {
  let usersShared = [];

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/uploads/fetchSharedUsers",
    data: { uploadId: uploadId },
    success: function (response) {
      usersShared = response;
    },
    async: false,
  });
  return usersShared.sharedTo;
}

function displaySharedUsersTable(id) {
  let sharedUsers = populateSharedUsers(id);
  console.log("shared users: ", sharedUsers);

  for (user = 0; user < sharedUsers.length; user++) {
    var userId = sharedUsers[user];
    console.log(userId);
    var name = getUserInfo(userId);

    if (!name) {
      continue;
    }
    const table = document
      .querySelector("#sharedUserTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var newUser = document.createTextNode(name.fullName);

    var action = `<a class='noDeco' href='#' onclick='deleteSharedUserModal("${id}", " ${userId}"); return false;'>Remove</a>`;

    cell1.appendChild(newUser);
    cell2.innerHTML = action;
    cell2.className = "alignCenter";
  }
}

function populateSharedFiles(userId) {
  let sharedFiles = [];
  $.ajax({
    type: "GET",
    url: "http://localhost:3000/uploads/fetchSharedToUser",
    data: { userId: userId },
    success: function (response) {
      sharedFiles = response;
    },
    async: false,
  });

  return sharedFiles;
}

function displaySharedFilesTable() {
  let loggedInUser = getLoginSession("userId");
  let sharedFiles = populateSharedFiles(loggedInUser);

  for (file = 0; file < sharedFiles.length; file++) {
    var label = sharedFiles[file].label;
    var fileName = sharedFiles[file].fileName;
    var author = getUserInfo(sharedFiles[file].userId);
    if (!author) {
      continue;
    }
    const table = document
      .querySelector("#sharedUploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFileName = document.createTextNode(fileName);
    var newAuthor = document.createTextNode(author.fullName);

    cell1.appendChild(newLabel);
    cell2.appendChild(newFileName);
    cell2.className = "alignCenter";
    cell3.appendChild(newAuthor);
    cell3.className = "alignCenter";
  }
}

function shareFile(uploadId, selectedUser) {
  console.log(uploadId);
  console.log(selectedUser);

  console.log(typeof uploadId);
  console.log(typeof selectedUser);

  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/uploads/share`,
    data: { uploadId: uploadId, userId: selectedUser },
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

function deleteSharedUser(uploadId, targetUser) {
  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/uploads/unshare`,
    data: { uploadId: uploadId, userId: targetUser },
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

function deleteSharedUserModal(uploadId, targetUser) {
  var modal = document.querySelector("#deleteSharedUserModal");
  var span = document.querySelector("#closeDeleteSharedUser");
  var btnConfirm = document.querySelector("#btnDeleteSharedUser");
  var btnCancel = document.querySelector("#btnCancelDeleteSharedUser");
  modal.style.display = "block";

  btnConfirm.onclick = function () {
    deleteSharedUser(uploadId, targetUser);
    modal.style.display = "none";
    location.reload();
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  span.onclick = function () {
    modal.style.display = "none";
  };
}

/***  for Chats page: ***/

function formatDate(date) {
  let dayOfMonth = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  month = month < 10 ? "0" + month : month;
  dayOfMonth = dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth;
  hour = hour < 10 ? "0" + hour : hour;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return (
    year +
    "-" +
    month +
    "-" +
    dayOfMonth +
    " " +
    hour +
    ":" +
    minutes +
    ":" +
    seconds
  );
}

function sendMessage(message) {
  var userId = getLoginSession("userId");
  var today = new Date();
  var chats = [];
  var chat = {
    chatId: Date.now(),
    sender: userId,
    message: message,
    date: formatDate(today),
  };

  $.ajax({
    type: "POST",
    url: "http://localhost:3000/chatlist",
    data: chat,
    success: function (response) {
      return false;
    },
  });
  return true;
}

function populateChats() {
  let chats;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/chatlist",
    success: function (response) {
      chats = response;
    },
    async: false, // <- this turns it into synchronous
  });
  return chats;
}

function removeChats() {
  var element = document.querySelector("#messages");
  element.innerHTML = "";
}

function displayAllChats() {
  let chats = populateChats();
  const messages = document.querySelector("#messages");
  console.log(chats);

  if (!chats) {
    messages.innerHTML = `<p class='msg-el'> Send message to someone....</p>`;
  } else {
    removeChats();
    for (let i = 0; i < chats.length; i++) {
      var messageBody = chats[i].message;

      var dateSent = chats[i].date;

      var messageElement = document.createElement("p");
      var sender = chats[i].sender;
      console.log("sender", sender);

      if (getUserInfo(sender) == null) {
        messageElement.innerHTML +=
          "[" + dateSent + "] " + "<i>Inactive User</i>" + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      } else if (sender == getLoginSession("userId")) {
        messageElement.innerHTML +=
          "[" + dateSent + "] " + "<b>You</b>" + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      } else {
        const name = getUserInfo(sender).fullName;
        messageElement.innerHTML +=
          "[" + dateSent + "] " + name + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      }
    }
  }
  setTimeout(displayAllChats, 3000);
}

function displayUsernameChatPage() {
  const loggedInUser = getUser();
  let userData;
  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/${loggedInUser._id}`,
    success: function (data) {
      console.log(data);
      if (data) {
        userData = data;
      }
    },
    async: false,
  });

  const spanName = document.getElementById("name");
  spanName.textContent = userData.fullName;
}

function addChatAjax() {
  const message = document.getElementById("message");
  const btnSend = document.getElementById("sendMessage");

  btnSend.onclick = function () {
    if (message.value !== "") {
      sendMessage(message.value);
      displayAllChats();
      message.value = "";
    } else {
      alert("Please add message");
    }
  };
}
