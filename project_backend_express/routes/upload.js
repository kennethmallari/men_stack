var express = require("express");
var router = express.Router();
var http = require("http");
var formidable = require("formidable");
var fs = require("fs");

const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";
const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("uploads");

router.get("/allUploads", function (req, res, next) {
  /* FETCH UPLOADS. */
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

router.get("/myUploads", function (req, res) {
  // FETCH UPLOADS BY USER ID
  collection
    .find({
      userId: Number(req.query.userId),
    })
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      console.log(req.query.userId);
      console.log("data:", res.data);
    });
});

router.get("/fetchSharedUsers", function (req, res) {
  // FETCH UPLOAD BY UPLOAD ID
  collection.findOne(
    {
      uploadId: Number(req.query.uploadId),
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

router.post("/", function (req, res) {
  /* ADD NEW UPLOAD */
  let form = new formidable.IncomingForm();

  form.parse(req, function (err, fields, files) {
    console.log("Upload ID:", fields.uploadId);
    console.log("By:", fields.userId);
    console.log("Label:", fields.label);
    console.log("FIELDS", fields);
    console.log("File:", files);

    let oldPath = files.fileUpload.filepath;
    let newPath = "../uploads/" + files.fileUpload.originalFilename;

    fs.readFile(oldPath, function (err, data) {
      if (err) throw err;

      fs.writeFile(newPath, data, function (err) {
        if (err) throw err;
        collection.insertOne(
          {
            uploadId: parseInt(fields.uploadId),
            label: fields.fileDescription,
            userId: parseInt(fields.userId),
            fileName: files.fileUpload.originalFilename,
            sharedTo: [],
          },
          function (err, result) {
            if (err) throw err;
            res.json(result);
          }
        );
      });
    });
  });
});

router.put("/", function (req, res, next) {
  /* UPDATE UPLOAD */
  console.log("upload id:", req.body.uploadId);
  console.log("type:", typeof req.body.uploadId);
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
      },
      {
        $set: {
          label: req.body.label,
        },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

router.delete("/", function (req, res) {
  /* DELETE  UPLOAD */
  collection
    .deleteOne({
      uploadId: Number(req.body.uploadId),
    })
    .then((result) => {
      res.json(result);
    });
  fs.unlinkSync("../uploads/" + req.body.fileName);
});

router.put("/unshare", function (req, res) {
  console.log("Unsharing file to user#:", req.body.uploadId);
  /* Unshare to User */
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
      },
      {
        $pull: { sharedTo: Number(req.body.userId) },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

router.put("/share", function (req, res) {
  /* Share to user  */
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
        sharedTo: { $ne: Number(req.body.userId) }, // { $not: { $elemMatch: [Number(req.body.userId)] } },
      },
      {
        $push: { sharedTo: parseInt(req.body.userId) },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

router.get("/fetchSharedToUser", function (req, res) {
  /* Fetch all shared uploads to user */
  collection
    .find({
      sharedTo: Number(req.query.userId),
    })
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      console.log(req.query.userId);
      console.log("data:", res.data);
    });
});
module.exports = router;
