var express = require("express");
var router = express.Router();
const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";
var usersList = "";

const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("users");

/* GET ALL Users */
router.get("/", function (req, res, next) {
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// GET Single User:
router.get("/:id", function (req, res, next) {
  collection.findOne(
    {
      _id: new ObjectId(req.params.id),
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//Get User Info by userId:

router.get("/api/:id", function (req, res, next) {
  collection.findOne(
    {
      userId: req.params.id,
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//ADDING a User:
router.post("/", function (req, res, next) {
  collection.insertOne(
    {
      userId: req.body.userId,
      fullName: req.body.fullName,
      email: req.body.email,
      password: req.body.password,
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//Updating a User
router.put("/:id", function (req, res, next) {
  collection
    .findOneAndUpdate(
      { _id: new ObjectId(req.params.id) },
      {
        $set: {
          fullName: req.body.fullName,
          email: req.body.email,
          // password: req.body.password,
        },
      },
      {
        upsert: true,
      }
    )
    .then((result) => res.json(result));
});

//DELETING a User:
// router.delete("/:id", function (req, res, next) {
//   collection.deleteOne({ _id: new ObjectId(req.body.id) }).then((result) => {
//     res.json(result);
//   });
// });

// Delete single user
router.delete("/:id", function (req, res, next) {
  collection.deleteOne(
    {
      _id: new ObjectId(req.params.id),
    },
    function (err, result) {
      if (err) throw err;
      res.send(result);
    }
  );
});

// Get single user via email and password
router.post("/auth", function (req, res, next) {
  collection.findOne(
    {
      email: req.body.email,
      password: req.body.password,
    },
    function (err, result) {
      console.log(result);
      if (result === null) {
        res.send({ status: "failed", data: null });
      } else {
        res.send({ status: "success", data: result });
      }
    }
  );
});

//Login:
router.post("/authenticate", function (req, res, next) {
  collection.findOne(
    {
      email: req.body.email,
      password: req.body.password,
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

module.exports = router;
