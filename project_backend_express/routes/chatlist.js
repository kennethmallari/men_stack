var express = require("express");
var router = express.Router();

const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";
var chatList = "";

const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("chats");

// GET all chats:
router.get("/", function (req, res, next) {
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// POST a Chat:
router.post("/", function (req, res, next) {
  collection.insertOne(
    {
      chatId: req.body.chatId,
      date: req.body.date,
      message: req.body.message,
      sender: req.body.sender,
    },
    function (err, result) {
      if (err) throw err;
      res.send(result);
    }
  );
});

module.exports = router;

// var MongoClient = require("mongodb").MongoClient;
// var url = "mongodb://localhost:27017/";

// var chatList = "";

// MongoClient.connect(url, function (err, db) {
//   if (err) throw err;
//   var dbo = db.db("communication");
//   dbo
//     .collection("chatlist")
//     .find({})
//     .toArray(function (err, result) {
//       if (err) throw err;
//       chatList = result;
//       console.log(result);
//     });
// });

// /* GET users listing. */
// router.get("/", function (req, res, next) {
//   res.send(chatList);
// });

// // Add a Chat:
// router.post("/", function (req, res, next) {
//   collection.insertOne(
//     {
//       chatId: req.body.chatId,
//       date: req.body.date,
//       message: req.body.message,
//       sender: req.body.sender,
//     },
//     function (err, result) {
//       if (err) throw err;
//       res.send(result);
//     }
//   );
// });

// module.exports = router;

//My old code:

// var express = require("express");
// var router = express.Router();

// const uploads = [
//   {
//     id: 1,
//     label: "Sales Report",
//     fileName: "sales-Sep2014.xls",
//   },
//   {
//     id: 2,
//     label: "Quarterly Summary",
//     fileName: "SummaryQ4-2014.ppt",
//   },
//   {
//     id: 3,
//     label: "Projection 2013-14",
//     fileName: "SalesProfitProjection.xls",
//   },
// ];

// router.get("/", function (req, res, next) {
//   res.send({ uploads });
// });

// module.exports = router;

// var express = require("express");
// var router = express.Router();

// const chats = [
//   {
//     id: 1,
//     sender: "Text User",
//     timestamp: "[2013-01-27 01:00:16]",
//     message: "Lorem Ipsum dolor sit amet",
//   },
//   {
//     id: 2,
//     sender: "Text User",
//     timestamp: "[2013-01-27 01:05:22]",
//     message: "Lorem Ipsum dolor sit amet alitr",
//   },
//   {
//     id: 3,
//     sender: "Anne Hunter",
//     timestamp: "[2013-01-27 01:11:14]",
//     message: "Lorem Ipsum dolor sit amet. At vero eos at",
//   },
// ];

// router.get("/", function (req, res, next) {
//   res.send({ chats });
// });

// module.exports = router;
